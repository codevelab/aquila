<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 19/02/15
 * Time: 20:09
 */

namespace aquila\model;


use aquila\core\blueprint\Loginable;

class Administrator implements Loginable{
    private $id;
    private $name;
    private $email;

    function __construct(){}

    public static function getPrimaryKeyColumn(){
        return "admin_id";
    }

    public static  function getDatabaseConnection(){
        return "main";
    }

    public static function getTable(){
        return "admins";
    }

    public static function getLoginNameColumn(){
        return "admin_email";
    }

    public static function getPasswordColumn()
    {
        return "admin_passwd";
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}