<?php namespace aquila\controller;

	use aquila\core\Controller;
	use aquila\core\View;

    class Index extends Controller{

		public function action_index() {
			$view = View::forge("layout/main");

            $view->content = "<h1>Hello world from Aquila!</h1>";
			return $view;
		}

	}