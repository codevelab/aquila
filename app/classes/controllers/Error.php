<?php namespace aquila\controller;

use aquila\core\Controller;

class Error extends Controller{

	public function action_index(){
		
	}

	public function action_default($errorcode){
		echo $errorcode;
	}

	public function action_404(){
		echo "<h1>404 - site not found</h1>";
	}
}