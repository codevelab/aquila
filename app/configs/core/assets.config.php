<?php
	return array(
		'css_dir' 	=> '/assets/css/',
		'fonts_dir' => '/assets/font/',
		'img_dir' 	=> '/assets/img/',	
		'js_dir' 	=> '/assets/js/',
		'video_dir' => '/assets/video/',
	);