<?php
	return array(
		// must have trailing / TODO improve routing with regex
		'_root_'  	=> 'index',  // The default route
		'_error_' 	=> 'error/default',
		'_404_'   	=> 'error/404',    // The main 404 route
	);