<?php use \aquila\core\Assets; ?>
<head>
	<title><?=(isset($title)) ? $title : "Framework" ?></title>
	
	<!-- Set the viewport width to device width for mobile -->
  	<meta name="viewport" content="width=device-width" />
    <meta charset="utf-8"/>

	<link href='http://fonts.googleapis.com/css?family=Exo+2:400,300italic,300,200italic,100italic,100,200' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<?=Assets::css("foundation.min"); ?>
	<?=Assets::css("main"); ?>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<?=Assets::js("vendor/modernizr"); ?>

</head>