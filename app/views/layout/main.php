<!DOCTYPE html>
<html>
	<?=(isset($head)) ? $head : \aquila\core\View::forge("template/head") ?>

	<body>

		<div class="coverbg">
	        <?=(isset($content)) ? $content : "" ?>
		</div>

		<?=(isset($footer)) ? $footer: \aquila\core\View::forge("template/footer") ?>

	</body>
</html>