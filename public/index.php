<?php
	/**
	 * Aquila is a lightweight, fast and easy-to-use PHP5 framework.
	 *
	 * @package    Aquila
	 * @version    1.0.1
	 * @author     Code Development Lab IVS
	 * @copyright  2014 - 2015 Code Development Lab IVS
	 * @link       http://codevelab.com
	 */

use aquila\core\Aquila;

require_once("../core/Aquila.php");
	Aquila::bootstrap();

	Aquila::handleRequest();

	exit();

?>