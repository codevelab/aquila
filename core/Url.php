<?php namespace aquila\core;

class Url {
	
	/**
	 * Placeholder for the URL path (example.com/controller/method/parameter/parameter)
	 * @var String
	 */
	private static $urlPath = null;
	
	/**
	 * Placeholder for each bit in the URL path
	 * @var array
	 */
	private static $urlBits = array();
    
    /**
     * Set the URL path
     * @param String the url path
     */
    public static function setPath($path){
		self::updateData($path, true);
	}

	/**
	 * Updates the global variables
	 * @param  boolean $override
	 * @return void
	 */
	private static function updateData($path = "", $override = false){
		if($override || self::$urlPath === null){
			self::resetData();
			if(!empty($path)){
				$urlPath = self::$urlPath = $path;
			} else {
				$urlPath = self::$urlPath = (isset($_SERVER['REQUEST_URI'])) ? trim($_SERVER['REQUEST_URI'], '/')."/" : '';
			}
			$bits = explode('/', $urlPath);
			foreach($bits as $bit){
				if(!empty($bit)){
					self::$urlBits[] = $bit;		
				}
			}
		}
	}

	/**
	 * Resets the global variables
	 * @return void
	 */
	private static function resetData(){
		self::$urlPath = null;
		self::$urlBits = array();
	}
	
	/**
	 * Gets data from the current URL
	 * @return Array
	 */
	public static function getData(){
		self::updateData();
		$data["path"] = self::$urlPath;
		$data["bits"] = self::$urlBits;
		return $data;
	}
	
	/**
	 * Returns the collection of bits in the URL
	 * @return Array
	 */
	public static function getBits(){
		self::updateData();
		return self::$urlBits;
	}
	
	/**
	 * Returns a defined bit from the URL
	 * @param  Integer $whichBit
	 * @return String or false
	 */
	public static function getBit($whichBit){
		self::updateData();
		return (self::hasBit($whichBit)) ? self::$urlBits[$whichBit] : false;
	}
	
	/**
	 * Returns the first bit in the path (the controller)
	 * @return String
	 */
	public static function getController(){
		self::updateData();
		return self::getBit(0);
	}

	/**
	 * Returns the second bit in the path (the method)
	 * @return String or false
	 */
	public static function getMethod(){
		self::updateData();
		return self::getBit(1);
	}

	/**
	 * Returns all parameters
	 * @return Array
	 */
	public static function getParameters(){
		self::updateData();
		$bits = array();
		if(self::getController() && self::getMethod()){
			$bits = self::getBits();
			unset($bits[0]);
			unset($bits[1]);
			$bits = array_values($bits); // reindex
		}
		return $bits;
	}

	/**
	 * Checks if a bit is present
	 * @param  Integer  $whichBit
	 * @return boolean
	 */
	private static function hasBit($whichBit){
		return (isset(self::$urlBits[$whichBit]));
	}

	/**
	 * Return the URL path (example: /this/is/the/path)
	 * @return String
	 */
	public static function getPath(){
		self::updateData();
		return self::$urlPath;
	}

	/**
	 * Returns the full URL (http://example.com/this/is/the/path)
	 * Note that this may have been affected by setPath()! 
	 * For the current page URL, see getCurrentUrl().
	 * @return String
	 */
	public static function getUrl(){
		self::updateData();
		return BASE_URL.self::$urlPath;
	}

	/**
	 * Returns the current URL (http://example.com/this/is/the/path)
	 * @return String
	 */
	public static function getCurrentUrl(){
		return (isset($_SERVER['HTTPS']) ? "https" : "http")."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	}

	/**
	 * Building a link to a page (Not fully implemented)
	 * @param  Array   $bits
	 * @param  string  $qs
	 * @param  boolean $admin
	 * @return String
	 */
	public static function buildURL(Array $bits, $qs = "", $admin = false ){
		$the_rest = '';
		foreach($bits as $bit){
			$the_rest .= $bit . '/';
		}
		$the_rest = ($qs != '') ? $the_rest . '?&' .$qs : $the_rest;
		
		return $the_rest;
	}

	public static function redirect($url){
		header("Location: " . $url);
		die();
	}

	public static function refresh(){
		header("Location: " . self::getCurrentUrl());
		die();
	}

	public static function back(){
		header("location:javascript://history.go(-1)");
		die();
	}

	public static function forward(){
		header("location:javascript://window.history.forward()");
		die();
	}
	
}