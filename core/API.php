<?php namespace aquila\core;

use stdClass;

class API {

    /**
     * @var counts number of calls to API
     */
    private static $count;

    /**
     * @var counts number of failed calls to API
     */
    private static $failedCount;

    /**
     * @var base url for the API call
     */
    private static $url;

    /**
     * @var PORT number to call on
     */
    private static $port;

    /**
     * @var Will be appended to the URL when building the call
     */
    private static $append;

    /**
     * @var Will be prepended to the URL when building the call
     */
    private static $prepend;

    /**
     * @var The expected responsetype (default: json)
     */
    private static $responseType;

    /**
     * @array Will be added as GET parameters to the url, ex user/pass
     */
    private static $getParams;

    /**
     * @array Will be posted with every call
     */
    private static $postParams;

    /**
     * @var Username to be used with "Basic Authentication" (HTTP header login)
     */
    private static $user;

    /**
     * @var Password to be used with "Basic Authentication" (HTTP header login)
     */
    private static $pass;

    /**
     * @var Last called URL
     */
    private static $lastURL;

    /**
     * @array Statuscodes which can be returned from the API
     */
    public static $statuses = [
        200 => "OK",
        201 => "Created",
        202 => "OK, Not completed",
        204 => "No content",
        400 => "Client-side error",
        401 => "Not Authorized",
        403 => "Understood, but failed",
        404 => "Not found",
        500 => "Internal server error"
    ];

    /**
     * @param string $url
     * @param int $port
     * Will setup the API - must be called before any post
     */
    public static function setup($url = "127.0.0.1", $port = 3000){
        self::setUrl($url);
        self::setPort($port);
        self::setResponseType("json");
        self::setDefaultGetParams();
        self::setDefaultPostParams();
        self::$count = 0;
        self::$failedCount = 0;
    }

    /**
     * @return counts
     */
    public static function getCount(){
        return self::$count;
    }

    /**
     * @return counts
     */
    public static function getFailedCount(){
        return self::$failedCount;
    }

    /**
     * @return PORT
     */
    public static function getPort(){
        return self::$port;
    }

    /**
     * @param $string
     * Sets the string to be appended
     */
    public static function appendUrl($string){
        self::$append = trim($string, "/");
    }

    /**
     * @param $string
     * Sets the string to be prepended
     */
    public static function prependUrl($string){
        self::$prepend = trim($string, "/")."/";
    }

    /**
     * @param $url
     * Sets the baseurl
     */
    public static function setUrl($url){
        self::$url = trim($url, "/")."/";
    }

    /**
     * @param $port
     * Sets the port
     */
    public static function setPort($port){
        if(is_int($port))
            self::$port = $port;
    }

    /**
     * @param $type
     * Sets the expected responsetype
     */
    public static function setResponseType($type){
        self::$responseType = $type;
    }

    /**
     * @array $params
     * Sets the default GET parameters (will always be added)
     */
    public static function setDefaultGetParams($array = array()){
        self::$getParams = $array;
    }

    /**
     * @array $params
     * Sets the default POST parameters (will always be added)
     */
    public static function setDefaultPostParams($array = array()){
        self::$postParams = $array;
    }

    /**
     * @string $string
     * Get the user from the "Basic Authentication"
     */
    public static function getUser(){
        return self::$user;
    }

    /**
     * @string $string
     * Set the user for "Basic Authentication"
     */
    public static function setUser($user){
        self::$user = $user;
    }

    /**
     * @string $pass
     * Set the password for "Basic Authentication"
     */
    public static function setPass($pass){
        self::$pass = $pass;
    }

    /**
     * @return Last
     */
    public static function getLastURL(){
        return self::$lastURL;
    }

    /**
     * @param $where
     * @param array $getParams
     * @return string
     * Builds the URL to be used by other internal methods
     */
    private static function buildUrl($where, $getParams = array()){
        $url = self::$url.self::$prepend.trim($where, "/").self::$append;
        if(!empty(self::$getParameters)){
            $url .= '?'.http_build_query(array_merge(self::$getParameters, $getParams));
        }
        return $url;
    }

    /**
     * @return resource
     * Builds and returns the curl object with default options.
     */
    private static function buildCurl(){
        $ch = curl_init();
        if(isset(self::$user) && isset(self::$pass)){
            curl_setopt($ch, CURLOPT_USERPWD, self::$user.":".self::$pass);
        }
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_PORT, self::$port);
        return $ch;
    }

    /**
     * @param $url
     * @param $ch curl object
     * @return array
     * @throws \Exception
     * Will perform the actual URL call
     */
    private static function handleRequest($url, $ch){
        self::$count++;
        self::$lastURL = $url;
        curl_setopt($ch, CURLOPT_URL, $url);
        if(!$result = curl_exec($ch)){
            self::$failedCount++;
            throw new \Exception(curl_error($ch), curl_getinfo($ch, CURLINFO_HTTP_CODE));
        }
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if(self::$responseType === "json"){
            $result = json_decode($result);
        }

        $obj = new stdClass();
        $obj->result = $result;
        $obj->status = new stdClass();
        $obj->status->code = $status;
        $obj->status->message = self::$statuses[$status];
        return $obj;
    }

    /**
     * @param $where
     * @param array $getParams
     * @return array
     * @throws \Exception
     * Used to make a GET request
     */
    public static function get($where, $getParams = array()){
        $url = self::buildUrl($where, $getParams);
        $ch = self::buildCurl();
        $result = self::handleRequest($url, $ch);
        return $result;
    }

    /**
     * @param $where
     * @param array $parameters
     * @return array
     * @throws \Exception
     * Used to make the POST request
     */
    public static function post($where, $parameters = array()){
        $url = self::buildUrl($where, self::$getParams);
        $ch = self::buildCurl();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);

        $result = self::handleRequest($url, $ch);

        return $result;
    }


    /**
     * @param $where
     * @param array $getParams
     * @return array
     * @throws \Exception
     * Used to make the POST request
     */
    public static function delete($where, $getParams = array()){
        $url = self::buildUrl($where, $getParams);
        $ch = self::buildCurl();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

        $result = self::handleRequest($url, $ch);

        return $result;
    }

    /**
     * @param $where
     * @param array $putParams
     * @return array
     * @throws \Exception
     * Used to make the POST request
     */
    public static function put($where, $putParams = array()){
        $url = self::buildUrl($where);
        $ch = self::buildCurl();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $putParams);

        $result = self::handleRequest($url, $ch);

        return $result;
    }

}