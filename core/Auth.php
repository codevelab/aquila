<?php namespace aquila\core;

use aquila\core\blueprint\Loginable;

class Auth {

	/**
	 * Should be called when you want to check if a user has logged in
	 * @return bool
     */
	public static function check(){
		$loggedIn = false;
		if(Session::get("framework_user") != null){
			$loggedIn = true;
		}
		return $loggedIn;
	}

	public static function hash($secret){
		return password_hash($secret, PASSWORD_DEFAULT); // PHP5 function
	}

	public static function login(Loginable $user, $loginName, $pass){
		$q = "SELECT {$user->getPasswordColumn()} AS passwd FROM {$user->getTable()} WHERE {$user->getLoginNameColumn()} = :login";

		DB::execute($q, array(
			":login" => $loginName
		));

		$row = DB::getRow();

		$shouldLogin = $row != null ? $row : null;

		$result = null;
		if($shouldLogin){
			if(password_verify($pass, $row->passwd)){
				Session::save("framework_user", $user);
				$result = true;
			}
		}

		return $result;
		
	}

	public static function getActiveUser(){
		$user = Session::get('framework_user');
		return $user;
	}

	public static function logout($redirecturl = false){
		Session::remove("framework_user");
		Url::redirect( ($redirecturl) ? $redirecturl : BASE_URL );
	}
}

?>