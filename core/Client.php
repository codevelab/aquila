<?php namespace aquila\core;


class Client {

    public static function getPublicIP(){
        return $_SERVER['REMOTE_ADDR'];
    }

    public static function getRealIP(){
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP)):
            $ip = $client;
        elseif(filter_var($forward, FILTER_VALIDATE_IP)):
            $ip = $forward;
        else:
            $ip = $remote;
        endif;

        return $ip;
    }

    public static function getLocation($ip = null){
        $ip = ($ip !== null && filter_var($ip, FILTER_VALIDATE_IP)) ? $ip : self::getRealIp();
        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
        return array(
            "hostname"  => (@isset($details->hostname) && $details->hostname !== "No Hostname") ? $details->hostname : false,
            "city"      => (@isset($details->city) && $details->city !== null) ? $details->city : false,
            "region"    => (@isset($details->region) && $details->region !== null) ? $details->region : false,
            "country"   => (@isset($details->country) && $details->country !== null) ? $details->country : false,
            "location"  => (@isset($details->loc) && $details->loc !== null) ? explode(",", $details->loc) : false,
            "org"       => (@isset($details->org) && $details->org !== null) ? $details->org : false,
        );
    }

}