<?php namespace aquila\core;

class Session {

	private static function init($force = false){
		if(session_status() == PHP_SESSION_NONE){
			session_start();
			Session::save("framework_flash", array());
		}
	} 

	public static function save($index, $element){
		self::init();
		$_SESSION[$index] = $element;
	}

	public static function get($index){
		self::init();
		$result = null;
		if(isset($_SESSION[$index])){
			$result = $_SESSION[$index];
		}
		return $result;
	}

	public static function flash($index, $element, $requests = 1){
		$flash = Session::get("framework_flash");
		array_push($flash, array($index => $element, "requests" => $requests));
		Session::save("framework_flash", $flash);
	}

	public static function clearFlash(){
		foreach(Session::get("framework_flash") as $k => $v){
			if($v["requests"] <= 0){
				unset($_SESSION["framework_flash"][$k]);
			} else {
				$_SESSION["framework_flash"][$k]["requests"]--;
			}
		}
	}

	public static function getFlash($index){
		$tmp = Session::get("framework_flash");
		$result = false;
		if(is_array($tmp)){
			foreach(Session::get("framework_flash") as $k => $v){
				if(array_key_exists($index, $tmp[$k])){
					$result = $tmp[$k][$index];
				}
			}
		}
		return $result;
	}

	public static function remove($index){
		self::init();
		$result = false;
		if(isset($_SESSION[$index])){
			unset($_SESSION[$index]);
			$result = true;
		}
		return $result;
	}

}

?>