<?php namespace aquila\core;

	use aquila\core\exception\HTTPException;

	class Route {

		private static $config = array();

		public static function process(){
			self::$config = array_merge(self::$config, Config::loadConfig('core','route'));

			// Check if routing rule should be applied
			if (array_key_exists(Url::getPath(), self::$config)) {
				Url::setPath(self::$config[Url::getPath()]);
			}

			$class = Request::getControllerName();
			$methodName = Request::getMethodName();
			$method = Request::getMethod();

			if(class_exists($class)){
				$controller = new $class();

				if(method_exists($controller, "action_" . $methodName)){
					$methodName = "action_" . $methodName;
				} else if($method == "POST" && method_exists($controller, "post_" . $methodName)){
					$methodName = "post_" . $methodName;
				} else if($method == "GET" && method_exists($controller, "get_" . $methodName)){
					$methodName = "get_" . $methodName;
				} else {
					throw new HTTPException(404, "Method not found action_/post_/get_" . $methodName . " in class " . $class);
				}

				$controller->before();
				$r = new \ReflectionMethod($class, $methodName);
				if(count(Url::getParameters()) >= $r->getNumberOfRequiredParameters()){
					$response = call_user_func_array(array($controller, $methodName), Url::getParameters());
					$response = new Response($response);
				} else {
					throw new HTTPException(404, "Parameter count did not match got " . count(Url::getParameters()) . " but method requires " . $r->getNumberOfRequiredParameters());
				}
			} else {
				throw new HTTPException(404, "Class '" . $class . "' not found with method '" . $methodName . "'");
			}

			return $response;
		}

		public static function addRoute($request, $route){
			self::$config[$request] = $route;
		}

		public static function getRoutes(){
			return self::$config;
		}

		public static function getRoute($route){
			return (self::hasRoute($route)) ? self::$config[$route] : null;
		}

		public static function hasRoute($route){
			return (array_key_exists($route, self::$config));
		}

	}