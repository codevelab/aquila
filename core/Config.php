<?php namespace aquila\core;

	class Config {

		public static function loadConfig($package, $className){
			return include(FRAMEWORK_PATH . 'app/configs/' . strtolower($package) . '/' . strtolower($className) . '.config'  . '.php');
		}
		
	}

?>