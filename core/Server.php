<?php namespace aquila\core;


class Server {

    public static function get($index = null){
        $return = $_SERVER;
        if($index !== null){
            $return = $_SERVER[$index];
        }
        return $return;
    }

    public static function getRequestMethod(){
        return $_SERVER["REQUEST_METHOD"];
    }

    public static function getProtocol(){
        return $_SERVER["SERVER_PROTOCOL"];
    }

    public static function getHostname(){
        return gethostname();
    }

    public static function getHost(){
        return $_SERVER["HTTP_HOST"];
    }

    public static function getSoftware(){
        return $_SERVER["SERVER_SOFTWARE"];
    }

    public static function getServerIP(){
        return $_SERVER["SERVER_ADDR"];
    }

}