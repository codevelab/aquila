<?php namespace aquila\core;

use aquila\controller;

class Request {

    private static $controllerName;
    private static $methodName;
    private static $method;

    public static function process(){

        self::$controllerName = (Url::getController()) ? ucfirst(strtolower(Url::getController())) : "Index"; //Controller to be called
        self::$controllerName = "\\aquila\\controller\\" . self::$controllerName;
        self::$methodName = (Url::getMethod()) ? Url::getMethod() : "index"; //Controller to be called
        self::$method = Server::getRequestMethod();

        return Route::process();
    }

    public static function getControllerName(){
        return self::$controllerName;
    }

    public static function getMethodName(){
        return self::$methodName;
    }

    public static function getMethod(){
        return self::$method;
    }

}