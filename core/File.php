<?php namespace aquila\core;

use SplFileObject;

class File extends SplFileObject {

	public static function load($file, $open_mode = "r", $use_include_path = false, $context = null) {
		return new static($file, $open_mode, $use_include_path, $context);
	}

	public function __construct($file, $open_mode = "r", $use_include_path = false, $context = null){
		parent::__construct($file, $open_mode, $use_include_path, $context);
	}

	/*
	Constants
	const integer DROP_NEW_LINE = 1 ;
	const integer READ_AHEAD = 2 ;
	const integer SKIP_EMPTY = 4 ;
	const integer READ_CSV = 8 ;

	Methods
	public __construct ( string $filename [, string $open_mode = "r" [, bool $use_include_path = false [, resource $context ]]] )
	public string|array current ( void )
	public bool eof ( void )
	public bool fflush ( void )
	public string fgetc ( void )
	public array fgetcsv ([ string $delimiter = "," [, string $enclosure = "\"" [, string $escape = "\\" ]]] )
	public string fgets ( void )
	public string fgetss ([ string $allowable_tags ] )
	public bool flock ( int $operation [, int &$wouldblock ] )
	public int fpassthru ( void )
	public int fputcsv ( array $fields [, string $delimiter = "," [, string $enclosure = '"' ]] )
	public mixed fscanf ( string $format [, mixed &$... ] )
	public int fseek ( int $offset [, int $whence = SEEK_SET ] )
	public array fstat ( void )
	public int ftell ( void )
	public bool ftruncate ( int $size )
	public int fwrite ( string $str [, int $length ] )
	public void getChildren ( void )
	public array getCsvControl ( void )
	public int getFlags ( void )
	public int getMaxLineLen ( void )
	public bool hasChildren ( void )
	public int key ( void )
	public void next ( void )
	public void rewind ( void )
	public void seek ( int $line_pos )
	public void setCsvControl ([ string $delimiter = "," [, string $enclosure = "\"" [, string $escape = "\\" ]]] )
	public void setFlags ( int $flags )
	public void setMaxLineLen ( int $max_len )
	public void __toString ( void )
	public bool valid ( void )

	Inherited methods
	public SplFileInfo::__construct ( string $file_name )
	public int SplFileInfo::getATime ( void )
	public string SplFileInfo::getBasename ([ string $suffix ] )
	public int SplFileInfo::getCTime ( void )
	public string SplFileInfo::getExtension ( void )
	public SplFileInfo SplFileInfo::getFileInfo ([ string $class_name ] )
	public string SplFileInfo::getFilename ( void )
	public int SplFileInfo::getGroup ( void )
	public int SplFileInfo::getInode ( void )
	public string SplFileInfo::getLinkTarget ( void )
	public int SplFileInfo::getMTime ( void )
	public int SplFileInfo::getOwner ( void )
	public string SplFileInfo::getPath ( void )
	public SplFileInfo SplFileInfo::getPathInfo ([ string $class_name ] )
	public string SplFileInfo::getPathname ( void )
	public int SplFileInfo::getPerms ( void )
	public string SplFileInfo::getRealPath ( void )
	public int SplFileInfo::getSize ( void )
	public string SplFileInfo::getType ( void )
	public bool SplFileInfo::isDir ( void )
	public bool SplFileInfo::isExecutable ( void )
	public bool SplFileInfo::isFile ( void )
	public bool SplFileInfo::isLink ( void )
	public bool SplFileInfo::isReadable ( void )
	public bool SplFileInfo::isWritable ( void )
	public SplFileObject SplFileInfo::openFile ([ string $open_mode = "r" [, bool $use_include_path = false [, resource $context = NULL ]]] )
	public void SplFileInfo::setFileClass ([ string $class_name ] )
	public void SplFileInfo::setInfoClass ([ string $class_name ] )
	public void SplFileInfo::__toString ( void )
	*/
}

?>