<?php namespace aquila\core;

use finfo;
use RuntimeException;

class Upload {

    public static $NO_FILE_SENT         = 1;
    public static $EXCEED_FILE_SIZE     = 2;
    public static $UNKNOWN_ERROR        = 3;
    public static $INVALID_FILE_FORMAT  = 4;

    private $uploadPath;
    private $files;

    public function __construct($path){
        $this->uploadPath = $path;
    }

    /**
     * @param array $file must have save params as $_FILES
     * @param array $validation
     */
    public function addFile($file, $validation = array()){
        $file["validation"] = $validation;
        $this->files[] = $file;
    }

    /**
     *
     */
    public function process(){
        $response = false;

        if(count($this->files) > 0){
            $response = array();
            reset($this->files);

            $file = current($this->files);

            self::validate($file); // throws error if not validated which will stop upload of file

            $filename = self::generateName($file["name"]);
            $response["path"] = $this->uploadPath.$filename;
            $response["filename"] = $filename;
            $response["validated"] = true;
            $response["success"] = false;
            if(!move_uploaded_file($file["tmp_name"], $this->uploadPath.$filename)){
                $response["success"] = true;
            }
        }

        return $response;
    }

    /**
     * @param $filename
     * @return string
     * Generates a random name and appends the fileextension
     */
    public static function generateName($filename){
        return rand(0,100).uniqid(true) . "." . pathinfo($filename, PATHINFO_EXTENSION);
    }

    /**
     * Validates a file
     * @param $file
     * @return bool
     * @throws \HttpInvalidParamException
     */
    public function validate($file){

        switch ($file['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('No file sent.', self::$NO_FILE_SENT);
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new RuntimeException('Exceeded filesize limit.', self::$EXCEED_FILE_SIZE);
            default:
                throw new RuntimeException('Unknown errors.', self::$UNKNOWN_ERROR);
        }

        $val = $file["validation"];
        if(isset($val["MAX_FILE_SIZE"]) && $val["MAX_FILE_SIZE"] <= $file["size"]){
            throw new RuntimeException("Exceeded filesize limit.", self::$EXCEED_FILE_SIZE);
        } else if(isset($val["file_exts"]) && is_array($val["file_exts"])){
            $valid = false;
            foreach($val["file_exts"] as $type){
                if(strtolower($type) == pathinfo(strtolower($file["name"]), PATHINFO_EXTENSION)){
                    $valid = true;
                }
            }
            if(!$valid){
                throw new RuntimeException("Extension did not match.", self::$INVALID_FILE_FORMAT);
            }
        } else if(isset($val["file_mime"])){
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === array_search($finfo->file($file['tmp_name']),$val["file_mime"],true)){
                throw new RuntimeException('Invalid file format.', self::$INVALID_FILE_FORMAT);
            }
        }

        return true;
    }

}