<?php

namespace aquila\core;

use finfo;
use RuntimeException;

class FileValidator {

    public static $NO_FILE_SENT         = 1;
    public static $EXCEED_FILE_SIZE     = 2;
    public static $UNKNOWN_ERROR        = 3;
    public static $INVALID_FILE_FORMAT  = 4;

    /**
     * Validates a file
     * @param $file
     * @param $val
     * @return bool
     * @throws RuntimeException
     */
    public static function validate($file, $val){

        switch ($file['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('No file sent.', self::$NO_FILE_SENT);
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new RuntimeException('Exceeded filesize limit.', self::$EXCEED_FILE_SIZE);
            default:
                throw new RuntimeException('Unknown errors.', self::$UNKNOWN_ERROR);
        }

        if(isset($val["MAX_FILE_SIZE"]) && $val["MAX_FILE_SIZE"] <= $file["size"]){
            throw new RuntimeException("Exceeded filesize limit.", self::$EXCEED_FILE_SIZE);
        } else if(isset($val["file_exts"]) && is_array($val["file_exts"])){
            $valid = false;
            foreach($val["file_exts"] as $type){
                if($type == pathinfo($file["name"], PATHINFO_EXTENSION)){
                    $valid = true;
                }
            }
            if(!$valid){
                throw new RuntimeException("Extension did not match.", self::$INVALID_FILE_FORMAT);
            }
        } else if(isset($val["file_mime"])){
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === array_search($finfo->file($file['tmp_name']),$val["file_mime"],true)){
                throw new RuntimeException('Invalid file format.', self::$INVALID_FILE_FORMAT);
            }
        }

        return true;
    }
}