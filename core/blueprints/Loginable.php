<?php namespace aquila\core\blueprint;


interface Loginable {

    public function getPrimaryKeyColumn();

    public function getDatabaseConnection();

    public function getTable();

    public function getLoginNameColumn();

    public function getPasswordColumn();

}