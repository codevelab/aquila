<?php namespace aquila\core; 

class Model extends \Illuminate\Database\Eloquent\Model{

    public function __get($what){
        if(!method_exists($this, $what) && isset($this->prefix)){
            if(!preg_match("/^(fk_|pivot)/", $what)){
                $what = $this->prefix.$what;
            }
        }
        return parent::__get($what);
    }

    public function __set($key, $value){
        if(isset($this->prefix)){
            if(!preg_match("/^(fk_|pivot)/", $key)){
                $key = $this->prefix.$key;
            }
        }
        parent::__set($key, $value);
    }

}