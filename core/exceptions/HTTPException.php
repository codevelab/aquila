<?php namespace aquila\core\exception;

use aquila\core\Response;

class HTTPException extends \Exception{

    private $debugmsg;

    // Redefine the exception so message isn't optional
    public function __construct($code = 404, $debugmsg = "", \Exception $previous = null) {
        // make sure everything is assigned properly
        parent::__construct(Response::$statuses[$code], $code, $previous);

        $this->debugmsg = $debugmsg;
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function getDebugMsg(){
        return $this->debugmsg;
    }

}