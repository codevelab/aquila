<?php namespace aquila\core;

	class Assets{

		private static $config = null;

		public static function init(){
			self::$config = Config::loadConfig("core","assets");
		}

		public static function css($fileName){
			self::init();
			$path = self::$config['css_dir'] . $fileName . '.css';
			return '<link rel="stylesheet" type="text/css" href="' . $path .'" media="screen" />';
		}

		//TODO Auto detect image type and add .ext automatically.
		public static function img($fileName){
			self::init();
			$path = self::$config['img_dir'] . $fileName;
			return '<img src="' . $path . '">';
		}

		public static function js($fileName){
			self::init();
			$path = self::$config['js_dir'] . $fileName . '.js';
			return '<script src="' . $path . '"></script>';
		}

	}