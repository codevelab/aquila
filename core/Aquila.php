<?php namespace aquila\core;

use aquila\core\exception\HTTPException;

class Aquila {

    const VERSION           = "1.0.1";

    private static $env     = Aquila::PRODUCTION;
    private static $config;

    const DEVELOPMENT       = 'development';
    const TESTING           = 'testing';
    const PRODUCTION        = 'production';

    const L_NONE            = 0;
    const L_ERROR           = 1;
    const L_WARNING         = 2;
    const L_DEBUG           = 3;
    const L_INFO            = 4;
    const L_ALL             = 5;

    private static $initialized = false;

    public static function bootstrap(){
        if(self::$initialized){
            throw new \Exception("You can't run Aquila bootstrap more than once.");
        }

        self::define();
        require_once(FRAMEWORK_PATH . "vendor/autoload.php");
        self::$config = Config::loadConfig("core", "aquila");
        self::initEnvironment();
        self::initDatabases();

        self::$initialized = true;
    }

    private static function initEnvironment(){
        self::setEnvironment(self::$config["environment"]);

        date_default_timezone_set("Europe/Copenhagen");
        setlocale(LC_MONETARY,"dk_DK");

        if(self::isDevelopment()){
            error_reporting(E_ALL);
            ini_set('display_errors', '1');
        }
    }

    public static function handleRequest(){
        $response = null;
        try {
            $response = Request::process();
        } catch(HTTPException $e){
            if($route = Route::getRoute("_{$e->getCode()}_")){
                Url::setPath($route);
            } else if($route = Route::getRoute("_error_")){
                Url::setPath($route . "/" . $e->getCode());
            }

            $response = Request::process();
            $response->set_status($e->getCode());
        }

        if($response instanceof Response){
            $response->send(true);
        }
    }

    private static function define(){
        define('FRAMEWORK_PATH', realpath(__DIR__.'/../').DIRECTORY_SEPARATOR);
        define('APPPATH', FRAMEWORK_PATH.'/app'.DIRECTORY_SEPARATOR);
        define('COREPATH', FRAMEWORK_PATH.'/core'.DIRECTORY_SEPARATOR);
        defined('START_TIME') or define('START_TIME', microtime(true));

        $protocol = 'http';
        if(isset($_SERVER['HTTPS'])){
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        }

        define('BASE_URL', $protocol . "://" . $_SERVER['HTTP_HOST'] . "/");
    }

    public static function setEnvironment($env){
        if(!($env === Aquila::PRODUCTION || $env === Aquila::DEVELOPMENT || $env === Aquila::TESTING)){
            throw new \Exception("Use only defined environment from Aquila class.");
        }
        self::$env = $env;
    }

    private static function initDatabases(){
        $db = Config::loadConfig("core", "database");
        if($db && count($db) > 0){ // check if config exists and connections exists

            $database = new Database();
            foreach($db as $key => $conn){
                $database->addConnection($conn, $key);
            }

            $database->setAsGlobal();
            $database->bootEloquent();
        }
    }

    public static function getEnvironment(){
        return self::$env;
    }

    public static function isProduction(){
        return (self::$env === self::PRODUCTION);
    }

    public static function isDevelopment(){
        return (self::$env === self::DEVELOPMENT);
    }

    public static function isTesting(){
        return (self::$env === self::TESTING);
    }

}