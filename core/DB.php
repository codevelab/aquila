<?php namespace aquila\core;

class DB {

    private static $current = null;
    private static $connections = array();

    private static $statement = "";
    private static $sth;
    
    /**
     * Adds a new connection to connections array
     * @param  string  $name     The connection name. 							Required
     * @param  string  $user     The user to login with.	 					Required
     * @param  string  $pass     Password for the above user. 					Required
     * @param  string  $database The name of the database. 						Required
     * @param  string  $host     Host of the database. 							Default: 127.0.0.1
     * @param  string  $driver   Type of connection (mysql,pgsql,...)       	Default: mysql
     * @param  string  $charset  Charset for database (utf8,latin..)          	Default: utf8
     * @param  boolean $default  use as default connection? 					Default: true
     * @return bool            	 Returns a boolean for success or failure
     */
    public static function newConnection($name, $user, $pass, $database, $host = "127.0.0.1", $driver = "mysql", $charset="utf8", $default = true){
        $success = false;
        if(!array_key_exists($name, self::$connections)){
            try{
                $dns = $driver.":dbname={$database};host={$host};charset={$charset}";
                self::$connections[$name] = new \PDO($dns, $user, $pass);
                self::$connections[$name]->setAttribute(1, $database);
                if(Aquila::isDevelopment()){
                    self::$connections[$name]->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                }
                $success = true;
            } catch (\PDOException $e){/*TODO Error handling*/}

            if($default === true){
                self::$current = $name;
            }
        }
        return $success;
    }

    /**
     * @param string $sql SQL to be executed
     * @return PDOStatement
     */
    public static function execute($sql = null, $parameters = array()){
        self::$sth = self::getConnection()->prepare(($sql === null) ? self::$statement : $sql);
        try{
            self::$sth->execute($parameters);
        } catch(SQLException $e){
            echo "ERROR";
        }
        return self::$sth;
    }

    /**
     * Returns resultset from executed statement
     * @param  int $method      PDO fetch type
     * @return Resultset        Resultset from statement
     */
    public static function getRows($method = \PDO::FETCH_OBJ){
        return self::$sth->fetchAll($method);
    }

    /**
     * Returns single result from executed statement
     * @param  int $method      PDO fetch type
     * @return Object           Result object from statement
     */
    public static function getRow($method = \PDO::FETCH_OBJ){
        return self::$sth->fetch($method);
    }

    /**
     * Check if a connection with a given name exists
     * @param  String  $name Name of connection
     * @return boolean       If it exists or not
     */
    private static function hasConnection($name){
    	return (array_key_exists($name, self::$connections));
    }

    /**
     * Get current connection
     * @return PDO The connection currently in use
     */
    public static function getConnection(){
    	return self::$connections[self::$current];
    }

    /**
     * Change current connection
     * @param String $name Name of connection to switch to
     */
    public static function setConnection($name = null){
    	if($name !== null && self::hasConnection($name)){
    		self::$current = $name;
    	}
    }


/* QUERYBUILDER */

    // DEBUG
    public static function printStatement(){
        echo self::$statement;
    }

    private static function buildRegex($regex){
        $regex = explode(",", $regex);
        $result = "/";
        $i = 0;
        foreach($regex as $value){
            $result .= "($value)".((count($regex) == ++$i) ? "" : "|");
        }

        return ($result .= "/");
    }

    public static function select($attributes = null, $prepend = null, $exclude = "^fk_"){

        // Build the regex
        $exclude = self::buildRegex($exclude);

        // Start select statement
        if($attributes !== null){
            self::$statement .= "SELECT ";
            if($prepend !== null){
                $i = 0;
                $attr = explode(",", $attributes);
                foreach($attr as $value){
                    $value = trim($value);
                    self::$statement .= ((preg_match($exclude, $value)) ? "" : $prepend) . $value . ((count($attr) == ++$i) ? " " : ", "); 
                }
            } else {
                self::$statement .= trim($attributes) . " ";
            }
        } else {
            self::$statement .= "* ";
        }

        return new self;
    }

    public function from($from){
        self::$statement .= "FROM " . trim($from) . " ";
        return $this;
    }

    public function join($table, $condition = ""){
        self::$statement .= "INNER JOIN $table ON " . self::escape($condition) . " ";
        return $this;
    }

    public function ljoin($table, $condition = ""){
        self::$statement .= "LEFT OUTER JOIN $table ON " . self::escape($condition) . " ";
        return $this;
    }

    public function rjoin($table, $condition = ""){
        self::$statement .= "RIGHT OUTER JOIN $table ON " . self::escape($condition) . " ";
        return $this;
    }

    public function fjoin($table, $condition = ""){
        self::$statement .= "FULL OUTER JOIN $table ON " . self::escape($condition) . " ";
        return $this;
    }

    public function njoin($table, $condition = ""){
        self::$statement .= "NATURAL JOIN $table ON " . self::escape($condition) . " ";
        return $this;
    }

    public function where($condition){
        self::$statement .= "WHERE (" . self::escape($condition) . ") ";
        return $this;
    }

    public function where_equals($a, $b){
        self::$statement .= "WHERE (" . self::escape($a) . " = " . self::escape($b) . ") ";
        return $this;
    }

    public function and_where($condition){
        self::$statement .= "AND (" . self::escape($condition) . ") ";
        return $this;
    }

    public function or_where($condition){
        self::$statement .= "OR (" . self::escape($condition) . ") ";
        return $this;
    }

    public function groupby($condition, $order = "ASC"){
        self::$statement .= "GROUP BY " . self::escape($condition) . " " . $order . " ";
        return $this;
    }

    public function limit($rows, $offset = null){
        self::$statement .= "LIMIT " . $rows . (($offset !== null) ? " OFFSET " . $offset : "") . " ";
        return $this;
    }

    public function _and(){
        self::$statement .= "AND ";
        return $this;
    }

    public function _or(){
        self::$statement .= "OR ";
        return $this;
    }

    public function add($sql){
        self::$statement .= trim($sql) . " ";
        return $this;
    }

    public static function escape($expr){
        return addslashes(trim($expr));
    }

}

?>