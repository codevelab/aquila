<?php namespace aquila\core;

class View{

	private $file = null;

	private static $globalData = array();
	private $data = array();

	public function __construct($file, $data = null){
		if($file !== null) {
			$this->setFile($file);
		}

		if(is_array($data)){
			$this->data = $data;
		}
	}

	public function __set($key, $value){
		$this->set($key, $value);
	}

	public function __get($key){
		return $this->get($key);
	}

	public function set($key, $value){
		$this->data[$key] = $value;
	}

	public function get($key = null, $default = null) {
		if (func_num_args() === 0 or $key === null) {
			$value = $this->data;
		} elseif (array_key_exists($key, $this->data)) {
			$value = $this->data[$key];
		} elseif (array_key_exists($key, static::$globalData)) {
			$value = static::$globalData[$key];
		} else if (is_null($default) and func_num_args() === 1) {
			throw new \OutOfBoundsException('View variable is not set: '.$key);
		} else {
			$value = $default;
		}

		return $value;
	}

	public static function setGlobal($key, $value){
		static::$globalData[$key] = $value;
	}

	public static function getGlobal($key, $default = null){
		$value = null;
		if (array_key_exists($key, static::$globalData)) {
			$value = static::$globalData[$key];
		} else if(!is_null($default)){
			$value = $default;
		}
		return $value;
	}

	public static function forge($file = null, $data = null) {
		return new static($file, $data);
	}

	public function render(){
		return $this->process_file();
	}

	public function setFile($file = null){
		$this->file = $file;
	}

	private function getData(){
		return array_merge($this->data, self::$globalData);
	}

	private function process_file(){
		extract($this->getData(), EXTR_REFS);
		ob_start();
		include (FRAMEWORK_PATH . "app/views/" . $this->file . ".php");
		return ob_get_clean();
	}
	
	public function __toString(){
		return $this->render();
	}

}